app.controller('payments', function($scope, Data) {
	
	$scope.$watch(function(){
    	return Data.paymentMethod;
    },function(){
    	$scope.paymentMethod=Data.paymentMethod;
    });
	
	$scope.$watch(function(){
    	return Data.payments;
    },function(){
    	var data = Data.payments;
    	if(data){
    		$scope.payments = data;
      	}
    });

});