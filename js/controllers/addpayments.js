app.controller('addpayments', function($scope, dopay, $mdToast) {
	$scope.pay = function(){
		var data = {
			'merchant':$scope.merchant, 
			'method':$scope.method, 
			'amount': $scope.amount,
			"currency": "USD"
		};
		var promise = dopay.payByIdeal(data);
	    promise.then(
	      function(payload) {
	        $scope.nForm.$setUntouched(true);
	        $mdToast.show($mdToast.simple().textContent('Thank You!'));
	      },
	      function(errorPayload) {
	        $mdToast.show($mdToast.simple().textContent('Failure saving payments!'));
	      }
	    );
		}
});