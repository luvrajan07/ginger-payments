app.controller('home', function($scope, Data, merchants) {
	$scope.getPaymentsByCallback = function() {
    Data.paymentMethod = null;
    Data.getPayments();
	}
	
	$scope.getPaymentsByPromise = function() {
    Data.paymentMethod = null;
    var promise = merchants.getGinger();
    promise.then(
      function(payload) {
        Data.payments = payload['data'];
      },
      function(errorPayload) {
        console.log('failure loading payments', errorPayload);
      }
    );
  }

  $scope.filterPaymentsByMethod = function(){
    Data.paymentMethod = 'ideal';
  }

});
