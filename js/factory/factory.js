app.factory('Data', function($http) {
    
    var self = {};
    self.init = false;
    self.paymentMethod = null;

    self.getPayments = function(){
    	$http.get('http://localhost:3000/payments?_sort=amount&_order=DESC&_limit=20').success(function(data) {
			self.payments = data;
      //console.log(data);
			return self.payments;
		});
    }
        
    return self;

});
app.factory('merchants', function($http, Data) {
    return {
      getGinger: function() {
        return $http.get('http://localhost:3000/payments?merchant=Ginger');
      }
    }
});

app.factory('dopay', function($http, $httpParamSerializer) {
    return {
      payByIdeal: function(postData) {
      	console.log("dopay");
         return $http({
		       withCredentials: false,
		       method: 'post',
		       url: 'http://localhost:3000/payments',
		       data: postData
		 });
      }
    }
});