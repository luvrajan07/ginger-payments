app.config(function($stateProvider, $urlRouterProvider, $httpProvider, $mdThemingProvider) {
    $mdThemingProvider.theme('default')
    .primaryPalette('light-blue')

    $urlRouterProvider.when('', '/home');
    $urlRouterProvider.otherwise('/home');
  //   RestangularProvider.setBaseUrl('http://api.openweathermap.org/data/2.5/weather');

    $stateProvider
        .state('dashboard', {
            url: "",
            templateUrl: 'partials/base.html',
            controller: 'base'
        })
        .state('dashboard.home', {
            url: "/home",
            views: {
                '': {
                    templateUrl: 'partials/home.html',
                    controller: 'home'
                }, 
                'payments@dashboard.home': {
                    templateUrl: 'partials/widgets/payments.html',
                    controller: 'payments'
                }
            }
        }) 
        .state('dashboard.addpayments', {
            url: "/addpayments",
            views: {
                '': {
                    templateUrl: 'partials/addpayments.html',
                    controller: 'addpayments'
                },
                'paymentform@dashboard.addpayments': {
                    templateUrl: 'partials/widgets/paymentform.html',
                    controller: 'addpayments'
                }
            }
        });
       
});


app.run(function ($rootScope, $state, Data) {

});
