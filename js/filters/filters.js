app.filter('paymentmethod', function(){
 return function(input, attribute) {
 	if (!angular.isObject(input)) return input;
 	if (!attribute) return input;
    var method;
    var posFilterArray = [];
    angular.forEach(input, function(payment){
        method = payment.method;
        if(method===attribute){
              posFilterArray.push(payment);
        }
    });
    return posFilterArray;
 }
});